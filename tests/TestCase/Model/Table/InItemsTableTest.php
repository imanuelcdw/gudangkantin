<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InItemsTable Test Case
 */
class InItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InItemsTable
     */
    public $InItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.InItems'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InItems') ? [] : ['className' => InItemsTable::class];
        $this->InItems = TableRegistry::getTableLocator()->get('InItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
