<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemInsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemInsTable Test Case
 */
class ItemInsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemInsTable
     */
    public $ItemIns;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ItemIns'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ItemIns') ? [] : ['className' => ItemInsTable::class];
        $this->ItemIns = TableRegistry::getTableLocator()->get('ItemIns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemIns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
