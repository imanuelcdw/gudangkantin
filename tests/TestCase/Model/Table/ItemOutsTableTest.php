<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemOutsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemOutsTable Test Case
 */
class ItemOutsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemOutsTable
     */
    public $ItemOuts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ItemOuts',
        'app.Items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ItemOuts') ? [] : ['className' => ItemOutsTable::class];
        $this->ItemOuts = TableRegistry::getTableLocator()->get('ItemOuts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemOuts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
