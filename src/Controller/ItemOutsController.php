<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * ItemOuts Controller
 *
 * @property \App\Model\Table\ItemOutsTable $ItemOuts
 *
 * @method \App\Model\Entity\ItemOut[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemOutsController extends AppController
{
    public function beforeRender(Event $event)
    {
        $fakepage = 'Barang Keluar';

        $this->set(compact('fakepage'));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Items']
        ];
        $itemOuts = $this->paginate($this->ItemOuts);

        $this->set(compact('itemOuts'));
    }

    /**
     * View method
     *
     * @param string|null $id Item Out id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemOut = $this->ItemOuts->get($id, [
            'contain' => ['Items']
        ]);

        $this->set('itemOut', $itemOut);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $itemOut = $this->ItemOuts->newEntity();
        if ($this->request->is('post')) {
            $itemOut = $this->ItemOuts->patchEntity($itemOut, $this->request->getData());
            if ($this->ItemOuts->save($itemOut)) {
                $this->Flash->success(__('The item out has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item out could not be saved. Please, try again.'));
        }

        $items = $this->ItemOuts->Items->find('list');
        $this->set(compact('itemOut', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Item Out id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemOut = $this->ItemOuts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemOut = $this->ItemOuts->patchEntity($itemOut, $this->request->getData());
            if ($this->ItemOuts->save($itemOut)) {
                $this->Flash->success(__('The item out has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item out could not be saved. Please, try again.'));
        }
        $items = $this->ItemOuts->Items->find('list', ['limit' => 1]);
        $this->set(compact('itemOut', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Item Out id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $itemOut = $this->ItemOuts->get($id);
        if ($this->ItemOuts->delete($itemOut)) {
            $this->Flash->success(__('The item out has been deleted.'));
        } else {
            $this->Flash->error(__('The item out could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function out($id = null){
        $itemOut = $this->ItemOuts->newEntity();
        if ($this->request->is('post')) {
            $itemOut = $this->ItemOuts->patchEntity($itemOut, $this->request->getData());
            if ($this->ItemOuts->save($itemOut)) {
                $this->Flash->success(__('The item out has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item out could not be saved. Please, try again.'));
        }
        $stocks = $this->ItemOuts->Items->find()->where(['id'=>$id]);

        foreach($stocks as $key){
            $stock = $key->stock;
            $name = $key->name;
        }

        $items = $this->ItemOuts->Items->find('list')->where(['id'=>$id]);
        $this->set(compact('itemOut', 'items', 'stock', 'name'));
    }
}
