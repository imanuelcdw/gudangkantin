<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * ItemIns Controller
 *
 * @property \App\Model\Table\ItemInsTable $ItemIns
 *
 * @method \App\Model\Entity\ItemIn[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemInsController extends AppController
{

    public function beforeRender(Event $event)
    {
        $fakepage = 'Barang Masuk';

        $this->set(compact('fakepage'));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $itemIns = $this->paginate($this->ItemIns);

        $this->set(compact('itemIns'));
    }

    /**
     * View method
     *
     * @param string|null $id Item In id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemIn = $this->ItemIns->get($id, [
            'contain' => []
        ]);

        $this->set('itemIn', $itemIn);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $itemIn = $this->ItemIns->newEntity();
        if ($this->request->is('post')) {
            $itemIn = $this->ItemIns->patchEntity($itemIn, $this->request->getData());
            if ($this->ItemIns->save($itemIn)) {
                $this->Flash->success(__('The item in has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item in could not be saved. Please, try again.'));
        }
        $items = TableRegistry::get('items')->find();
        $this->set(compact('itemIn','items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Item In id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemIn = $this->ItemIns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemIn = $this->ItemIns->patchEntity($itemIn, $this->request->getData());
            if ($this->ItemIns->save($itemIn)) {
                $this->Flash->success(__('The item in has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item in could not be saved. Please, try again.'));
        }
        $this->set(compact('itemIn'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Item In id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $itemIn = $this->ItemIns->get($id);
        if ($this->ItemIns->delete($itemIn)) {
            $this->Flash->success(__('The item in has been deleted.'));
        } else {
            $this->Flash->error(__('The item in could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
