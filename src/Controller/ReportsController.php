<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Reports Controller
 *
 *
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReportsController extends AppController
{

    public function beforeRender(Event $event)
    {
        $report = 'report';

        $this->set(compact('report'));
    }

    public function reportIn(){
        $page = 'Laporan barang masuk';
        $subpage = null;

        $this->loadModel('ItemIns');
        $items = $this->ItemIns->find();


        $this->set(compact('page','subpage','items'));
    }

    public function reportOut(){
        $page = 'Laporan barang keluar';
        $subpage = null;

        $this->set(compact('page','subpage'));
    }

    public function print(){
        
    }
}
