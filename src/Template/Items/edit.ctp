<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($item) ?>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control','type'=>'text']) ?>    
                </div>
                
                <div class="form-group">
                    <?= $this->Form->control('stock',['class'=>'form-control']) ?>    
                </div>
            </div>
        </div>

        
        <?= $this->Form->button('Simpan',['class'=>'btn btn-warning']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
