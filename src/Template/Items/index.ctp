<!-- CSS -->
<?php $this->start('css') ?>
    <?= $this->Html->css('../modules/datatables/datatables.min.css') ?>
    <?= $this->Html->css('../modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') ?>
    <?= $this->Html->css('../modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') ?>
<?php $this->end() ?>

<!-- JS -->
<?php $this->start('script') ?>
    <?= $this->Html->script('../modules/datatables/datatables.min.js') ?>
    <?= $this->Html->script('../modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') ?>
    <?= $this->Html->script('../modules/datatables/Select-1.2.4/js/dataTables.select.min.js') ?>
    <?= $this->Html->script('../modules/jquery-ui/jquery-ui.min.js') ?>
    <?= $this->Html->script('page/modules-datatables.js') ?>
<?php $this->end() ?>


<div class="card">
    <div class="card-header">
        <a href="<?= $this->Url->Build(['controller'=>'item-ins','action'=>'add']) ?>" class="btn btn-warning"><i class="fa fa-plus"></i> Input Barang</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped" id="table-1" border="0">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Stock</th>
                <th>Created</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
                <?php $no = 1 ?>
                <?php foreach($items as $value): ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $value->name ?></td>
                        <td><?= $value->stock.' kg' ?></td>
                        <td><?= $value->created ?></td>
                        <td>
                            <a href="<?= $this->Url->Build(['action'=>'edit',$value->id]) ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Barang"><i class="fa fa-edit"></i></a>
                            <a href="<?= $this->Url->Build(['action'=>'delete',$value->id]) ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Barang"><i class="fa fa-trash"></i></a>
                            <a href="<?= $this->Url->Build(['action'=>'view',$value->id]) ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Liat Barang"><i class="fa fa-eye"></i></a>
                            <a href="<?= $this->Url->Build(['controller'=>'item_outs','action'=>'out',$value->id]) ?>" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Keluarkan Barang"><i class="fa fa-sign-out-alt"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
          </table>
        </div>

    </div>
</div>

<?php $this->start('script') ?>
    <script>
        toastr.success('Success messages');
    </script>
<?php $this->end(); ?>


