<!-- CSS -->
<?php $this->start('css') ?>
    <?= $this->Html->css('../modules/datatables/datatables.min.css') ?>
    <?= $this->Html->css('../modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') ?>
    <?= $this->Html->css('../modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') ?>
<?php $this->end() ?>

<!-- JS -->
<?php $this->start('script') ?>
    <?= $this->Html->script('../modules/datatables/datatables.min.js') ?>
    <?= $this->Html->script('../modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') ?>
    <?= $this->Html->script('../modules/datatables/Select-1.2.4/js/dataTables.select.min.js') ?>
    <?= $this->Html->script('../modules/jquery-ui/jquery-ui.min.js') ?>
    <?= $this->Html->script('page/modules-datatables.js') ?>
<?php $this->end() ?>

<div class="card">
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <h1><?= h($item->name) ?></h1>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Id</label>
                    <div class="col-sm-9">
                        <?= h($item->id) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Stock</label>
                    <div class="col-sm-9">
                        <?= h($item->stock) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Created</label>
                    <div class="col-sm-9">
                        <?= h($item->created) ?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h1>Barang Masuk</h1>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" border="0">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Qty</th>
                            <th>Created</th>
                          </tr>
                        </thead>
                        <tbody>
                             <?php $no = 1 ?>
                            <?php if(!empty($item_ins)): ?>
                                <?php foreach($item_ins as $value): ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->qty.' kg' ?></td>
                                        <td><?= $value->created ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3"><center>Kosong</center></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h1>Barang Keluar</h1>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" border="0">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Qty</th>
                            <th>Created</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1 ?>
                            <?php if(!empty($item->item_outs)): ?>
                                <?php foreach($item->item_outs as $value): ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->qty.' kg' ?></td>
                                        <td><?= $value->created ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3"><center>Kosong</center></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
