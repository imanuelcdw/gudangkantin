
<div class="card">
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <h1><?= h($itemIn->name) ?></h1>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Id</label>
                    <div class="col-sm-9">
                        <?= h($itemIn->id) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Qty</label>
                    <div class="col-sm-9">
                        <?= h($itemIn->qty).' Kg' ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Price</label>
                    <div class="col-sm-9">
                        <?= 'Rp.'.$this->Number->format($itemIn->price) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Created</label>
                    <div class="col-sm-9">
                        <?= h($itemIn->created) ?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

