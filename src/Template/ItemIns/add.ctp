<!-- CSS -->
<?php $this->start('css') ?>
     <?= $this->Html->css('easy-autocomplete.min.css') ?>
     <?= $this->Html->css('easy-autocomplete.themes.min.css') ?>
<?php $this->end() ?>

<!-- JS -->
<?php $this->start('script') ?>
    <?= $this->Html->script('jquery.easy-autocomplete.min.js') ?>

    <script>
        var options = {
            data: [
                <?php foreach($items as $value): ?>
                    '<?= $value->name ?>',
                <?php endforeach; ?>
            ],
            list: {
                match: {
                    enabled: true
                }
            }
        };

        $("#name").easyAutocomplete(options); 
    </script>
<?php $this->end() ?>

<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($itemIn) ?>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control','type'=>'text','autocomplete'=>'off']) ?>    
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('qty',['class'=>'form-control']) ?>    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('price',['class'=>'form-control']) ?>    
                </div>
            </div>
        </div>

        
        <?= $this->Form->button('Simpan',['class'=>'btn btn-warning']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
