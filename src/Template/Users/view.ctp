
<div class="card">
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <h1><?= h($user->name) ?></h1>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Username</label>
                    <div class="col-sm-9">
                        <?= h($user->username) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Password</label>
                    <div class="col-sm-9">
                        <?= h($user->password) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Created</label>
                    <div class="col-sm-9">
                        <?= h($user->created) ?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

