<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($user) ?>
        
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control','type'=>'text']) ?>    
                </div>
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('username',['class'=>'form-control']) ?>    
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('password',['class'=>'form-control']) ?>    
                </div>
            </div>
        </div>

        
        <?= $this->Form->button('Update',['class'=>'btn btn-warning']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
