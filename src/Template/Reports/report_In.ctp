<div class="card">
	<div class="card-header">
        <a id="print" href="#" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>&nbsp;
        <a href="#" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>&nbsp;
        <a href="#" class="btn btn-danger"><i class="fa fa-print"></i> PDF</a>
    </div>
	<div class="card-body">
		<div class="table-responsive">
          <table class="table table-striped" id="table-1" border="0">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Created</th>
              </tr>
            </thead>
            <tbody>
                <?php $no = 1 ?>
                <?php foreach($items as $value): ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $value->name ?></td>
                        <td><?= $value->qty.' kg' ?></td>
                        <td><?= $value->price ?></td>
                        <td><?= $value->created ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
          </table>
        </div>
	</div>
</div>