
<div class="card">
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <h1><?= $itemOut->has('item') ? $this->Html->link($itemOut->item->name, ['controller' => 'Items', 'action' => 'view', $itemOut->item->id]) : '' ?></h1>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Id</label>
                    <div class="col-sm-9">
                        <?= h($itemOut->id) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Qty</label>
                    <div class="col-sm-9">
                        <?= h($itemOut->qty).' Kg' ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="col-sm-2">Created</label>
                    <div class="col-sm-9">
                        <?= h($itemOut->created) ?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

