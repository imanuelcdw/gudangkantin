<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($itemOut) ?>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('item_id',['class'=>'form-control','options'=>$items]) ?>    
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('qty',['class'=>'form-control']) ?>    
                </div>
            </div>
        </div>
        
        <?= $this->Form->button('Simpan',['class'=>'btn btn-warning']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
