<!-- Sidebar -->
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
            <div class="sidebar-brand">
                <a href="#">Gudang Kantin</a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="#">GK</a>
            </div>
            <ul class="sidebar-menu">
                <!-- Main Menu -->
                <li class="menu-header">Dashboard</li>
                <!-- <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="dashboard-general.html">General Dashboard</a></li>
                        <li><a class="nav-link" href="dashboard-ecommerce.html">Ecommerce Dashboard</a></li>
                    </ul>
                </li> -->

                <li><a href="<?= $this->Url->Build(['controller'=>'Pages','action'=>'dashboard']) ?>" class="nav-link <?= $page == 'dashboard' ? 'text-warning' : '' ?>"><i class="fas fa-fire"></i><span>Dashboard</span></a></li>

                <li><a href="<?= $this->Url->Build(['controller'=>'Users','action'=>'index']) ?>" class="nav-link <?= $page == 'users' ? 'text-warning' : '' ?>"><i class="fas fa-users"></i><span>Users</span></a></li>

                

                <li class="menu-header">Main</li>

                <li><a href="<?= $this->Url->Build(['controller'=>'Items','action'=>'index']) ?>" class="nav-link <?= $page == 'items' ? 'text-warning' : '' ?>"><i class="fas fa-cubes"></i><span>Items</span></a></li>

                <li><a href="<?= $this->Url->Build(['controller'=>'Item_ins','action'=>'index']) ?>" class="nav-link <?= $page == 'item-ins' ? 'text-warning' : '' ?>"><i class="fas fa-sign-in-alt"></i><span>Barang Masuk</span></a></li>

                <li><a href="<?= $this->Url->Build(['controller'=>'Item_outs','action'=>'index']) ?>" class="nav-link <?= $page == 'item-outs' ? 'text-warning' : '' ?>"><i class="fas fa-sign-out-alt"></i><span>Barang Keluar</span></a></li>                
				

                <li class="menu-header">Reports</li>
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown <?= !empty($report) ? 'text-warning' : '' ?>"><i class="fas fa-fire"></i><span>Laporan</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link <?= $page == 'Laporan barang masuk' ? 'text-warning' : '' ?>" href="<?= $this->Url->Build(['controller'=>'Reports','action'=>'report_In']) ?>">Laporan Barang Masuk</a></li>
                        <li><a class="nav-link <?= $page == 'Laporan barang keluar' ? 'text-warning' : '' ?>" href="<?= $this->Url->Build(['controller'=>'Reports','action'=>'report_Out']) ?>">Laporan Barang Keluar</a></li>
                    </ul>
                </li>


            </ul>

        </aside>
      </div>